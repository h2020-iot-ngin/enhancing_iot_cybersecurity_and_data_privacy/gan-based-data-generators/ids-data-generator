import seaborn as sns
import matplotlib.pyplot as plt


def correlation_matrix(input, path):
    corr = input.corr()
    sns.set(rc={'figure.figsize': (9.7, 7.7)})
    sns.set(rc={'figure.figsize':(9.7,7.7)})
    ax = sns.heatmap(
        corr,
        vmin=-1, vmax=1, center=0,
        cmap=sns.diverging_palette(220, 10, as_cmap=True),
        square=True
    )
    ax.set_xticklabels(
        ax.get_xticklabels(),
        rotation=45,
        horizontalalignment='right'
    )
    ax.set(xlabel=None)
    ax.set(ylabel=None)
    plt.savefig(path, bbox_inches='tight')
    plt.clf()