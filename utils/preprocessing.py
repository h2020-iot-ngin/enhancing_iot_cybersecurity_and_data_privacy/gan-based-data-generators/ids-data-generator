import pandas as pd

_INTRINSIC = [
    'duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment', 'urgent'
]
_CONTENT = [
    'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root',
    'num_file_creations',
    'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login', 'is_guest_login'
]
_TIME_BASED = [
    'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
    'srv_rerror_rate', 'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate'
]
_HOST_BASED = [
    'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate', 'dst_host_diff_srv_rate',
    'dst_host_same_src_port_rate',
    'dst_host_srv_diff_host_rate', 'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
    'dst_host_srv_rerror_rate']


def features():
    # features
    dos = ['back', 'land', 'neptune', 'pod', 'smurf', 'teardrop']
    r2l = ['ftp_write', 'guess_passwd', 'imap', 'multihop', 'phf', 'spy', 'warezclient', 'warezmaster']
    u2r = ['buffer_overflow', 'loadmodule', 'perl', 'rootkit']
    probe = ['ipsweep', 'nmap', 'portsweep', 'satan']

    # functional features
    ff = _INTRINSIC + _TIME_BASED

    # non-functional features
    nff = _CONTENT + _HOST_BASED

    return ff, nff


def read_KDD(path_dataset, path_names):
    # read the data
    real_data = pd.read_csv(path_dataset)

    df_col = pd.read_csv(path_names)
    # Making column names lower case, removing spaces
    df_col['Name'] = df_col['Name'].apply(lambda x: x.strip().replace(' ', '').lower())

    real_data = real_data.iloc[:, :-1]

    # Renaming our dataframe with proper column names
    real_data.columns = df_col['Name']  # real data, containing the class name of each attack

    real_data['su_attempted'] = real_data['su_attempted'].replace(2, 0)
    # print("real_data: ", real_data)

    return real_data


def select_class(real_data, attack_type):
    real_data = real_data.copy(deep=True)
    labeldf = real_data['class']

    # Change the label column
    # 0: normal
    # 1: DoS
    # 2: Probe
    # 3: R2L
    # 4: U2R

    # Change the label column
    newlabeldf = labeldf.replace(
        {'normal': 0, 'neptune': 1, 'back': 1, 'land': 1, 'pod': 1, 'smurf': 1, 'teardrop': 1, 'mailbomb': 1,
         'apache2': 1, 'processtable': 1, 'udpstorm': 1, 'worm': 1,
         'ipsweep': 2, 'nmap': 2, 'portsweep': 2, 'satan': 2, 'mscan': 2, 'saint': 2
            , 'ftp_write': 3, 'guess_passwd': 3, 'imap': 3, 'multihop': 3, 'phf': 3, 'spy': 3, 'warezclient': 3,
         'warezmaster': 3, 'sendmail': 3, 'named': 3, 'snmpgetattack': 3, 'snmpguess': 3, 'xlock': 3, 'xsnoop': 3,
         'httptunnel': 3,
         'buffer_overflow': 4, 'loadmodule': 4, 'perl': 4, 'rootkit': 4, 'ps': 4, 'sqlattack': 4, 'xterm': 4})

    # put the new label column back
    real_data['class'] = newlabeldf

    if attack_type == 'DoS':
        to_drop = [0, 1]
    elif attack_type == 'Probe':
        to_drop = [0, 2]
    elif attack_type == 'R2L':
        to_drop = [0, 3]
    elif attack_type == 'U2R':
        to_drop = [0, 4]

    real_data_attack_normal = real_data[real_data['class'].isin(to_drop)]

    # keep only the attack and the normal data
    if attack_type == 'DoS':
        real_attack = real_data_attack_normal[real_data_attack_normal["class"] == 1]
    elif attack_type == 'Probe':
        real_attack = real_data_attack_normal[real_data_attack_normal["class"] == 2]
    elif attack_type == 'R2L':
        real_attack = real_data_attack_normal[real_data_attack_normal["class"] == 3]
    elif attack_type == 'U2R':
        real_attack = real_data_attack_normal[real_data_attack_normal["class"] == 4]
    real_normal = real_data[real_data["class"] == 0]

    return real_normal, real_attack


def split_features(dataframe, selected_attack_class):
    functionnal_features = dataframe
    non_functionnal_features = remove_intrinsic(dataframe)

    if selected_attack_class == 'DoS':
        functionnal_features = remove_content(functionnal_features)
        functionnal_features = remove_host_based(functionnal_features)
        non_functionnal_features = remove_time_based(non_functionnal_features)

    return functionnal_features, non_functionnal_features


def remove_content(dataframe):
    """
    Removes all content features from the dataframe.
    """
    return dataframe.drop(columns=_CONTENT).reset_index(drop=True)


def remove_time_based(dataframe):
    """
    Removes all time based features from the dataframe.
    """
    return dataframe.drop(columns=_TIME_BASED).reset_index(drop=True)


def remove_host_based(dataframe):
    """
    Removes all host based features from the dataframe.
    """
    return dataframe.drop(columns=_HOST_BASED).reset_index(drop=True)


def remove_intrinsic(dataframe):
    """
    Removes all host based features from the dataframe.
    """
    return dataframe.drop(columns=_INTRINSIC).reset_index(drop=True)
