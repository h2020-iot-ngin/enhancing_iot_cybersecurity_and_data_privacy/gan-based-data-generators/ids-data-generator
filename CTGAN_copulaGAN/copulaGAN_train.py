import sys
sys.path.append("..")

import os
from sdv.tabular import CopulaGAN
from utils.preprocessing import read_KDD, select_class, features, split_features

# disable warnings
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter("ignore", category=ConvergenceWarning)

# path to NLS-KDD dataset
path_dataset = '../data/KDDTrain+.csv'
path_names = "../data/Field Names.csv"

# features
_INTRINSIC = [
    'duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment', 'urgent'
]
_CONTENT = [
    'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root',
    'num_file_creations',
    'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login', 'is_guest_login'
]
_TIME_BASED = [
    'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
    'srv_rerror_rate', 'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate'
]
_HOST_BASED = [
    'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate', 'dst_host_diff_srv_rate',
    'dst_host_same_src_port_rate',
    'dst_host_srv_diff_host_rate', 'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
    'dst_host_srv_rerror_rate']

# read the data
real_data = read_KDD(path_dataset, path_names)

# DoS attacks
attack_type = "DoS"
real_normal, real_attacks = select_class(real_data, attack_type)
functionnal_features, non_functionnal_features = split_features(real_attacks, attack_type)
normal_ff, normal_nff = split_features(real_normal, attack_type)

# functional and non-functional features
ff, nff = features()

# In the copulaGAN, we provide all the real attacks and we define the fields that need to be modeled and included in
# the generated output data (nff)
print("len of nff: ", len(nff))

field_dist = {
    'dst_host_count': 'gamma',
    'dst_host_srv_count': 'gamma',
    'dst_host_same_srv_rate': 'beta',
    'dst_host_diff_srv_rate': 'beta',
    'dst_host_same_src_port_rate': 'beta',
    'dst_host_srv_diff_host_rate': 'gamma',
    'dst_host_serror_rate': 'beta',
    'dst_host_srv_serror_rate': 'beta',
    'dst_host_rerror_rate': 'gamma',
    'dst_host_srv_rerror_rate': 'gamma'
}

# data type
data_type = {
    'logged_in': {
        "type": "categorical"
    },
    'root_shell': {
        "type": "categorical"
    },
    'su_attempted': {
        "type": "categorical"
    },
    'is_host_login': {
        "type": "categorical"
    },
    'is_guest_login': {
        "type": "categorical"
    }
}

# copulaGAN model
copulagan = CopulaGAN(field_names=nff, field_types=data_type, field_distributions=field_dist, epochs=100, verbose=True, discriminator_steps=5)
copulagan.fit(real_attacks)

# save the model
if not os.path.exists('../models'):
    os.makedirs('../models')
model_path = '../models/copula_kdd.pkl'
copulagan.save(model_path)
print("Model saved successfully.")


