import sys
sys.path.append("..")

import os
from utils.preprocessing import read_KDD, select_class, features, split_features
from sdv.tabular import CTGAN

# disable warnings
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter("ignore", category=ConvergenceWarning)

# path to NLS-KDD dataset
path_dataset = '../data/KDDTrain+.csv'
test_path_dataset = '../data/KDDTest+.csv'
path_names = "../data/Field Names.csv"
# read the data
real_data = read_KDD(path_dataset, path_names)
# test data
real_test_data = read_KDD(test_path_dataset, path_names)

# DoS attacks
attack_type = "DoS"
real_normal, real_attacks = select_class(real_data, attack_type)
# real_attacks.to_csv("real_attacks.csv")
functionnal_features, non_functionnal_features = split_features(real_attacks, attack_type)
normal_ff, normal_nff = split_features(real_normal, attack_type)

n_DoS = len(non_functionnal_features)
print("Number of DoS attacks: ", len(non_functionnal_features))

# functional and non-functional features
ff, nff = features()

# data type
data_type = {
    'logged_in': {
        "type": "categorical"
    },
    'root_shell': {
        "type": "categorical"
    },
    'su_attempted': {
        "type": "categorical"
    },
    'is_host_login': {
        "type": "categorical"
    },
    'is_guest_login': {
        "type": "categorical"
    }
}

# CTGAN model
ctgan = CTGAN(field_names=nff, field_types=data_type, verbose=True, epochs=100, discriminator_steps=5, rounding=2)
ctgan.fit(real_attacks)

# save the model
if not os.path.exists('../models'):
    os.makedirs('../models')
model_path = '../models/ctgan_kdd.pkl'
ctgan.save(model_path)
print("Model saved successfully.")
