import sys
sys.path.append("..")

import warnings
import pandas as pd
import os
from sdv.tabular import CopulaGAN
import sdmetrics
from utils.table_evaluator import TableEvaluator
from utils.correlation_matrix_vis import correlation_matrix
from utils.preprocessing import read_KDD, select_class, features, split_features

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings("ignore")

# Load data
path_dataset = '../data/KDDTrain+.csv'  # Train
test_path_dataset = '../data/KDDTest+.csv'  # Test
path_names = "../data/Field Names.csv"

# features
_INTRINSIC = [
    'duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment', 'urgent'
]
_CONTENT = [
    'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root',
    'num_file_creations',
    'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login', 'is_guest_login'
]
_TIME_BASED = [
    'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate',
    'srv_rerror_rate', 'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate'
]
_HOST_BASED = [
    'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate', 'dst_host_diff_srv_rate',
    'dst_host_same_src_port_rate',
    'dst_host_srv_diff_host_rate', 'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate',
    'dst_host_srv_rerror_rate']

# read the data
real_data = read_KDD(path_dataset, path_names)
# test data
real_test_data = read_KDD(test_path_dataset, path_names)

# DoS attacks
attack_type = "DoS"
# Train dataset
real_normal, real_attacks = select_class(real_data, attack_type)
functionnal_features, non_functionnal_features = split_features(real_attacks, attack_type)
non_functionnal_features = non_functionnal_features.drop("class", 1)

# Test dataset
test_real_normal, test_real_attacks = select_class(real_test_data, attack_type)
test_functionnal_features, test_non_functionnal_features = split_features(test_real_attacks, attack_type)
test_non_functionnal_features = test_non_functionnal_features.drop("class", 1)

n_test_attacks = len(test_non_functionnal_features)
print("Number of records in test dataset of DoS attack: ", n_test_attacks)

# functional and non-functional features
ff, nff = features()

# data type
data_type = {
    'logged_in': {
        "type": "categorical"
    },
    'root_shell': {
        "type": "categorical"
    },
    'su_attempted': {
        "type": "categorical"
    },
    'is_host_login': {
        "type": "categorical"
    },
    'is_guest_login': {
        "type": "categorical"
    }
}

field_dist = {
    'dst_host_count': 'gamma',
    'dst_host_srv_count': 'gamma',
    'dst_host_same_srv_rate': 'beta',
    'dst_host_diff_srv_rate': 'beta',
    'dst_host_same_src_port_rate': 'beta',
    'dst_host_srv_diff_host_rate': 'gamma',
    'dst_host_serror_rate': 'beta',
    'dst_host_srv_serror_rate': 'beta',
    'dst_host_rerror_rate': 'gamma',
    'dst_host_srv_rerror_rate': 'gamma'
}

# load the CopulaGAN model
copulagan = CopulaGAN(field_names=nff, field_types=data_type, field_distributions=field_dist, epochs=100, verbose=True, discriminator_steps=5)
loaded = copulagan.load('../models/copula_kdd.pkl')

# generate synthetic dataset
synthetic_data = loaded.sample(n_test_attacks)

# ----------------------------------------------------------------------------------
# Creation of full syntetic attacks
test_real_attacks.reset_index(drop=True, inplace=True)
synthetic_data.reset_index(drop=True, inplace=True)
synthetic_attacks = pd.concat([test_real_attacks[_INTRINSIC], synthetic_data[_CONTENT], test_real_attacks[_TIME_BASED],
                               synthetic_data[_HOST_BASED]], axis=1)
class_syntetic = [1] * n_test_attacks
synthetic_attacks['class'] = class_syntetic
synthetic_attacks.to_csv("../synthetic_dataset/copulaGAN_syn_attacks.csv")

# Test dataset consists of synthetic dataset and normal records of test dataset
test_dataset = synthetic_attacks.append(test_real_normal)
test_dataset = test_dataset.sample(frac=1).reset_index(drop=True)
n_test = len(test_dataset)

# ----------------------------------------------------------------------------------
# train dataset - real data
train_dataset = real_normal.append(real_attacks)
train_dataset = train_dataset.sample(n=n_test)

# ----------------------------------------------------------------------------------
# create the folder to save the images
path = "../results/copulaGAN"
if not os.path.exists(path):
    os.makedirs(path)

# ----------------------------------------------------------------------------------
# # Visual evaluation of the data
# Distributions of features and Cumulative Sum
table_evaluator = TableEvaluator(non_functionnal_features, synthetic_data)
table_evaluator.visual_evaluation(path)

# Correlation matrices
# eliminate columns with zero values
zero_column = ['num_failed_logins', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations', 'num_shells',
               'num_access_files', 'num_outbound_cmds', 'is_host_login', 'is_guest_login']
nff_corr = non_functionnal_features.drop(zero_column, axis=1)
correlation_matrix(nff_corr, path + "/real_nff_corr.png")

syn_corr = synthetic_data.drop(zero_column, axis=1)
correlation_matrix(syn_corr, path + "/syn_nff_corr.png")

print("The visual evaluation is done. The images are saved in: ", path)

# ----------------------------------------------------------------------------------
# Statistical metrics
# metadata table contains the type of each feature
metadata_table = {
    'fields':
        {
            'hot': {"type": 'numerical'},
            'num_failed_logins': {"type": 'numerical'},
            'logged_in': {"type": 'categorical'},
            'num_compromised': {"type": 'numerical'},
            'root_shell': {"type": 'categorical'},
            'su_attempted': {"type": 'categorical'},
            'num_root': {"type": 'numerical'},
            'num_file_creations': {"type": 'numerical'},
            'num_shells': {"type": 'numerical'},
            'num_access_files': {"type": 'numerical'},
            'num_outbound_cmds': {"type": 'numerical'},
            'is_host_login': {"type": 'categorical'},
            'is_guest_login': {"type": 'categorical'},
            'dst_host_count': {"type": 'numerical'},
            'dst_host_srv_count': {"type": 'numerical'},
            'dst_host_same_srv_rate': {"type": 'numerical'},
            'dst_host_diff_srv_rate': {"type": 'numerical'},
            'dst_host_same_src_port_rate': {"type": 'numerical'},
            'dst_host_srv_diff_host_rate': {"type": 'numerical'},
            'dst_host_serror_rate': {"type": 'numerical'},
            'dst_host_srv_serror_rate': {"type": 'numerical'},
            'dst_host_rerror_rate': {"type": 'numerical'},
            'dst_host_srv_rerror_rate': {"type": 'numerical'},
        }
}

logistic_detection = sdmetrics.single_table.LogisticDetection.compute(non_functionnal_features, synthetic_data, metadata_table)
print("logistic_detection: ", logistic_detection)

# ----------------------------------------------------------------------------------
# Machine Learning-Based Metrics
decision_tree = sdmetrics.single_table.BinaryDecisionTreeClassifier.compute(train_dataset, test_dataset, target='class')
print("decision_tree: ", decision_tree)
ada = sdmetrics.single_table.BinaryAdaBoostClassifier.compute(train_dataset, test_dataset, target='class')
print("ada: ", ada)
log_regr = sdmetrics.single_table.BinaryLogisticRegression.compute(train_dataset, test_dataset, target='class')
print("log_regr: ", log_regr)
mlp_class = sdmetrics.single_table.BinaryMLPClassifier.compute(train_dataset, test_dataset, target='class')
print("mlp_class: ", mlp_class)