import sys
sys.path.append("..")

import pandas as pd
import os
from sdgym.synthesizers import tablegan
from utils.preprocessing import read_KDD, select_class, features, split_features

# path to NLS-KDD dataset
path_dataset = '../data/KDDTrain+.csv'
path_names = "../data/Field Names.csv"

# read the data
real_data = read_KDD(path_dataset, path_names)

# functional and non-functional features
ff, nff = features()

# DoS attacks
attack_type = "DoS"
real_normal, real_attacks = select_class(real_data, attack_type)
functionnal_features, non_functionnal_features = split_features(real_attacks, attack_type)
normal_ff, normal_nff = split_features(real_normal, attack_type)

n_DoS = len(non_functionnal_features)
print("Number of DoS attacks: ", len(non_functionnal_features))

# Categorical columns
categorical_columns = ('logged_in', 'root_shell', 'su_attempted', 'is_host_login', 'is_guest_login')

# try to add categorical columns to fit
tablegan = tablegan.TableGAN(epochs=100)
tablegan.fit(non_functionnal_features, categorical_columns=categorical_columns)

# save the model
if not os.path.exists('../models'):
    os.makedirs('../models')
model_path = '../models/tablegan_kdd.pkl'
tablegan.save(model_path)
print("Model saved successfully.")

# save the synthetic nff
synthetic_data = tablegan.sample(n_DoS)
if not os.path.exists('../synthetic_dataset'):
    os.makedirs('../synthetic_dataset')

synthetic_data = pd.DataFrame(synthetic_data[:, :-1], columns=nff)
synthetic_data.to_csv("../synthetic_dataset/tablegan_syn_nff.csv")